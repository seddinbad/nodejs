var http = require('http');
var url = require('url');

const PORT = process.env.PORT || 8080;

var server = http.createServer(function(req,res){
	var page = url.parse(req.url).pathname;
	res.writeHead('200', {"Content-Type":"text/html"});
	res.write('<h1>hey</h1>');
	res.end();
});

server.listen(PORT, () => {
	console.log('Server listening on port ' + PORT);
});